<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Billetera
 * 
 * @property int $id
 * @property int $idcliente
 * @property int $saldo
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Cliente $cliente
 *
 * @package App\Models
 */
class Billetera extends Model
{
	protected $table = 'billetera';

	protected $casts = [
		'idcliente' => 'int',
		'saldo' => 'int'
	];

	protected $fillable = [
		'idcliente',
		'saldo'
	];

	public function cliente()
	{
		return $this->belongsTo(Cliente::class, 'idcliente');
	}

	
}

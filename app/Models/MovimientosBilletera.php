<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MovimientosBilletera
 * 
 * @property int $id
 * @property int $idcliente
 * @property float $valor
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Cliente $cliente
 *
 * @package App\Models
 */
class MovimientosBilletera extends Model
{
	protected $table = 'movimientos_billetera';
	public $incrementing = false;

	protected $casts = [
		'id' => 'int',
		'idcliente' => 'int',
		'valor' => 'float'
	];

	protected $fillable = [
		'id',
		'idcliente',
		'valor'
	];

	public function cliente()
	{
		return $this->belongsTo(Cliente::class, 'idcliente');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MovimientosCompra
 * 
 * @property int $id
 * @property int $idcliente
 * @property int $valor
 * @property int $created_at
 * @property int $updated_at
 *
 * @package App\Models
 */
class MovimientosCompra extends Model
{
	protected $table = 'movimientos_compras';

	protected $casts = [
		'idcliente' => 'int',
		'valor' => 'int',
		'created_at' => 'int',
		'updated_at' => 'int'
	];

	protected $fillable = [
		'idcliente',
		'valor'
	];
}

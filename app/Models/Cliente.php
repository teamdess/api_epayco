<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


/**
 * Class Cliente
 * 
 * @property int $id
 * @property string $documento
 * @property string $nombre
 * @property string $email
 * @property string $celular
 * 
 * @property Collection|Billetera[] $billeteras
 * @property MovimientosBilletera $movimientos_billetera
 *
 * @package App\Models
 */
class Cliente extends Model
{
	use Notifiable;
	protected $table = 'clientes';
	public $timestamps = false;

	protected $fillable = [		
		'documento',
		'nombre',
		'email',
		'celular'
	];

	public function billeteras()
	{
		return $this->hasMany(Billetera::class, 'idcliente');
	}

	public function movimientos_billetera()
	{
		return $this->hasOne(MovimientosBilletera::class, 'idcliente');
	}

	
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Billetera;
use App\Models\Cliente;

class BilleteraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $billetera = Billetera::all();
        return json_encode($billetera);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function recargarBilletera(Request $request){
        $response = [];
        $body = json_decode($request->getContent());

        //Busco cliente con los datos a registrar
        $cliente = Cliente::where('documento', $body->datos->documento)->where('celular', $body->datos->celular)->first();
        if ($cliente){            
           //Busco su billetera para modificar el saldo           
           $billetera = Billetera::where('idcliente', $cliente->id)->first();
           $billetera->saldo = $billetera->saldo + $body->datos->valor;
           $billetera->save();
            
           $response=[
            "description" => "Recarga realizada con éxito"
           ];
        }
        else{
            $response=[
                "description" => "Datos no registrados"
             ];
        }
        return json_encode($response);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ( Billetera::create($request->all())){
            $response=[
                "description" => "Success"
             ];
              
          }else{
            $response=[
               "description" => "Not Success"
           ];
              
          }

        return $response;
    }


    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function consultarsaldoBilletera(Request $request){
        $response = [];

        $body = json_decode($request->getContent());

        //Busco cliente con los datos a registrar
        $cliente = Cliente::where('documento', $body->datos->documento)->where('celular', $body->datos->celular)->first();
        if ($cliente){            
           //Busco su billetera para modificar el saldo
           
           $billetera = Billetera::where('idcliente', $cliente->id)->first();
            
           $response=[
            "description" => "Su saldo es" . " " . $billetera->saldo
           ];
        }
        else{
            $response=[
                "description" => "Datos no registrados"
             ];
        }
        return json_encode($response);
    }
}

<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\User;
use Notification;
//use App\Notifications\PagarNotification;
use App\Models\Cliente;

class NotificationController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth');
    }
  
    public function index()
    {
        return view('product');
    }
    
    public function sendPagoNotification() {
        echo 'entra';

        $userSchema = Cliente::first();
        $datospago = [
            'name' => 'Usuario',
            'body' => 'Usted ha recibido una solicitud de pago.',
            'thanks' => 'Gracias',
            'pagarText' => 'Verifique en la siguiente url',
            'pagoUrl' => url('/'),
            'pago_id' => 007
        ];
  
        Notification::send($userSchema, new PagarNotification($datospago));
   
        dd('Finalizado!');
    }
}
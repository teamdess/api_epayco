<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;
use App\Models\Billetera;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $cliente = Cliente::all();
        return json_encode($cliente);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $datos = [
                "documento" => $request->datos['documento'],
                "nombre" => $request->datos['nombre'],
                "celular" => $request->datos['celular'],
                "email" => $request->datos['email']
            ];
        $cliente = Cliente::create($datos);
          if ($cliente!=null){
            //Creo automáticamente la billetera del cliente
            $datos = [
                "idcliente" => $cliente->id,
                "saldo" => 0
            ];
            Billetera::create($datos);
            $response=[
                "description" => "Success"
             ];
              
          }else{
            $response=[
               "description" => "Not Success"
           ];
              
          }
        return $response;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = [];
        $cliente= Cliente::findOrFail( $id );

            if ( $cliente->update($request->datos)){
              $response=[
                  "description" => "Success"
               ];
                
            }else{
              $response=[
                 "description" => "Not Success"
             ];                
            }
            return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::findOrFail($id);
        if($cliente->delete()){
          return "Cliente eliminado con éxito!!";
        }else{
           return "Error al eliminar";
        }
    }
}

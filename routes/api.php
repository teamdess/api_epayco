<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NotificationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signUp');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});


Route::apiResource('clientes', 'ClientesController');
Route::delete('deleteclientes/{id}/{usu}', 'ClientesController@destroy');
Route::put('updatecclientes', 'ClientesController@modificar');
Route::get('buscarinfoclientesedit/{id}/{usu}', 'ClientesController@finDatosEdit');

Route::apiResource('billetera', 'BilleteraController');
Route::post('updatebilletera', 'BilleteraController@recargarBilletera');
Route::post('consultarsaldoBilletera', 'BilleteraController@consultarsaldoBilletera');

Route::get('/sendNotification', 'NotificationController@sendPagoNotification');

//Route::post('login', 'LoginController@do')->name('login');